import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
// Bootstrap & Reactstrap
import 'bootstrap/dist/css/bootstrap.css'

// Store
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './store/reducers/rootReducer';
// To bind Redux with React
import { Provider } from 'react-redux';
// Thunk
import thunk from 'redux-thunk';
// Firebase API's
import { reduxFirestore, getFirestore } from 'redux-firestore';
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase';
import fbConfig from './config/fbConfig';

// App
import App from './App';
import './styles/App.scss';

const store = createStore(rootReducer,
  compose(
    applyMiddleware(thunk.withExtraArgument({getFirebase, getFirestore})),
    reduxFirestore(fbConfig),
    reactReduxFirebase(fbConfig, {useFirestoreForProfile: true, userProfile: 'user', attachAuthIsReady: true}),
  )
);

store.firebaseAuthIsReady.then(() => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
  </Provider>, document.getElementById('root'));
  registerServiceWorker();
})


