import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import { CSSTransition, TransitionGroup } from 'react-transition-group';
import routes from './routes';
import Navigation from '../components/ui/Navigation';

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>

          <Navigation />

          <Route render={({location}) => (
            // <TransitionGroup>
            //   <CSSTransition
            //     key={location.key}
            //     timeout={300}
            //     classNames="fade"
            //   >
                <Switch location={location}>
                  {routes.map((route, i) => <Route key={i} {...route} exact/>)}
                </Switch>
            //   </CSSTransition>
            // </TransitionGroup>
          )} />

        </div>
      </BrowserRouter>
    );
  }
}

export default Router