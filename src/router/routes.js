// IMPORT ROUTES
import Home from '../views/Home';
import ProjectDetails from '../views/ProjectDetails';
import SignIn from '../components/auth/SignIn';
import SignUp from '../components/auth/SignUp';
import CreateProject from '../components/projects/CreateProject';
import ContextProvider from '../views/ContextProvider';
import ContextConsumer from '../views/ContextConsumer';

// DEFINE ROUTE PROPERTIES IN ALPHABETICAL ORDER
const routes= [
	{ path: '/', component: Home, exact: true },
	{ path: '/projects/:id', component: ProjectDetails },
	{ path: '/signin', component: SignIn },
	{ path: '/signup', component: SignUp },
	{ path: '/create', component: CreateProject },
	{ path: '/context-provider', component: ContextProvider },
	{ path: '/context-consumer', component: ContextConsumer},
];

// EXPORT OBJECT
export default routes;