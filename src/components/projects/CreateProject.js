import React, { Component } from 'react'

import { createProject } from '../../store/actions/projectActions';
import { connect } from 'react-redux';

import { Redirect } from 'react-router-dom';

class CreateProject extends Component {
  state = {
    title: ``,
    content: ``,
    isActive: ``,
    imgUrl: ``,
  }


  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    // console.log(this.state);
    this.props.createProject(this.state)
    this.props.history.push('/');
  }

  render() {

    const { auth } = this.props;
    if (!auth.uid) return <Redirect to='/signin' />

    return (
      <div className="form page">
        <form onSubmit={this.handleSubmit}>
          <h4 className="form__title">Create New Project</h4>
          <input
            type="text"
            id="title"
            onChange={this.handleChange}
            placeholder="Title"
            className="form__input"
            required
          />
          <textarea
            type="text"
            id="content"
            onChange={this.handleChange}
            placeholder="Content"
            className="form__input"
            rows="10"
            required
          />
          <input
            type="text"
            id="imgUrl"
            onChange={this.handleChange}
            placeholder="Image Url"
            className="form__input"
          />

          <button className="form__cta">Post it!</button>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  // console.log(state)
  return {
    auth: state.firebase.auth,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createProject: (project) => dispatch(createProject(project)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateProject);

