import React from 'react';
import moment from 'moment';

const ProjectSummary = ({project}) => {
  return (
    <div className="card">
      <h3 className="card__title">{project.title}</h3>
      <p className="card__summary">{project.content}</p>
      {project.imgUrl ? <img src={project.imgUrl} alt="Post" className="card__img" /> : null }
      <small className="card__info">Created At {moment(project.createdAt.toDate()).calendar()}</small>
      <small className="card__info">Published by {project.authorFirstName}</small>
    </div>
  )
}

export default ProjectSummary;