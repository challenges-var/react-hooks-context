import React from 'react';
import ProjectSummary from './ProjectSummary';
import { Link } from 'react-router-dom';

const ProjectList = ({projects}) => {
  return (
    <div className="project-list">
      {projects && projects.map(project => {
        return(
          <Link to={`/projects/${project.id}`} key={project.id} className="project-list__item">
            <ProjectSummary project={project} />
          </Link>
        )
      })}

    </div>
  )
}

export default ProjectList;
