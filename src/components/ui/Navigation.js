import React from 'react';
import { Link } from 'react-router-dom';
import SignedInLinks from '../layout/SignedInLinks';
import SignedOutLinks from '../layout/SignedOutLinks';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faReact } from '@fortawesome/free-brands-svg-icons';

import { connect } from 'react-redux';


const Navigation = (props) => {

  const { auth, profile } = props;
  // console.log(auth);
  const links = auth.uid ?  <SignedInLinks profile={profile} /> : <SignedOutLinks />;
  return (
    <nav className="nav">
      <div className="group nav__inner">
        <Link to="/"><FontAwesomeIcon icon={faReact} className="nav__icon"/></Link>
        <div className="nav__links">
          { links }
        </div>
      </div>
    </nav>
  )

}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
  }
};

export default connect(mapStateToProps)(Navigation);