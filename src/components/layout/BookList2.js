import React, { Component } from 'react';
import { ThemeContext } from '../../context/ThemeContext';

import axios from 'axios';

export class BookList2 extends Component {

  state = {
    posts: [],
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then((resp) => {
        // console.log(resp.data)
        this.setState({
          posts: resp.data.slice(0, 10),
        })
        console.log(this.state.posts)
      })
  }

  render() {

    const { posts } = this.state;
    const postList = posts.length ? (
      posts.map(post => {
        return (
          <div
            key={post.id}

            className="list__item"
          >
            <h3>{post.title}</h3>
            <p>{post.body}</p>
          </div>
        )
      })
    ) : (
      <div className="center">No posts to show</div>
    );

    return (
      <ThemeContext.Consumer>{(context) => {
        const { isLightTheme, light, dark} = context;
        const theme = isLightTheme ? light : dark;
        return (
          <div
            className="list"
            style={
              {
                background: theme.ui,
                color: theme.syntax,
              }
            }
          >
            {postList}
          </div>
        )
      }}
      </ThemeContext.Consumer>
    )
  }
}


export default BookList2;
