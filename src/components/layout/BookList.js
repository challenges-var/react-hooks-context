import React, { Component } from 'react';
import { ThemeContext } from '../../context/ThemeContext';

import axios from 'axios';


export class BookList extends Component {
  static contextType = ThemeContext;

  state = {
    posts: [],
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then((resp) => {
        // console.log(resp.data)
        this.setState({
          posts: resp.data.slice(0, 10),
        })
        // console.log(this.state.posts)
      })
  }

  render() {

    // console.log(this.context);
    const { isLightTheme, light, dark} = this.context;
    const theme = isLightTheme ? light : dark;


    const { posts } = this.state;
    const postList = posts.length ? (
      posts.map(post => {
        return (
          <div
            key={post.id}
            style={
              {
                background: theme.bg,
              }
            }
            className="list__item"
          >
            <h3>{post.title}</h3>
            <p>{post.body}</p>
          </div>
        )
      })
    ) : (
      <div className="center">No posts to show</div>
    );

    return (
      <div
        className="list"
        style={
          {
            background: theme.ui,
            color: theme.syntax,
          }
        }
      >
        {postList}
      </div>
    )
  }
}


export default BookList;
