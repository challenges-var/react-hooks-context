import React, { Component } from 'react';
import { ThemeContext } from '../../context/ThemeContext';

export class ThemeToggle extends Component {
  static contextType = ThemeContext
  state = {
    bgColor: '#cc5500'
  }

  render() {
    const { toggleTheme } = this.context;
    const handleClick = () => {
      toggleTheme();
      if(this.state.bgColor === '#cc5500') {
        this.setState({
          bgColor: 'grey'
        })
      } else {
        this.setState({
          bgColor: '#cc5500'
        })
      }
    }
    return (
      <div>
        <button
          onClick={handleClick}
          className="toggle-btn"
          style={{backgroundColor: this.state.bgColor}}
        >
          Toggle Theme
        </button>
      </div>
    )
  }
}

export default ThemeToggle;
