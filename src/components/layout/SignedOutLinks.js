import React from 'react';
import { NavLink } from 'react-router-dom';

const SignedOutLinks = () => {
  return (
    <ul className="links">
      <li><NavLink to="/signup" className="links__item">Sign Up</NavLink></li>
      <li><NavLink to="/signin" className="links__item">Log In</NavLink></li>
    </ul>
  )
}

export default SignedOutLinks;