import React from 'react';
import { NavLink } from 'react-router-dom';

import  { connect } from 'react-redux';
import { signOut } from '../../store/actions/authActions';

const SignedInLink = (props) => {
  // handlelogOut = () => {
  //   this.props.signOut;
  //   this.props.history.push('/signin');
  // }
  return (
    <ul className="links">
      <li><NavLink to="/context-provider" className="links__item" activeClassName="links__active">Context Provider</NavLink></li>
      <li><NavLink to="/context-consumer" className="links__item" activeClassName="links__active">Context Consumer</NavLink></li>
      <li><NavLink to="/create" className="links__item" activeClassName="links__active">New Project</NavLink></li>
      <li><NavLink to="/" className="links__item" onClick={props.signOut}>Log Out</NavLink></li>
      <li className="links__item links__state" >{props.profile.initials}</li>
    </ul>
  )
}

const mapDispatchTopProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}

export default connect(null, mapDispatchTopProps)(SignedInLink);