import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn } from '../../store/actions/authActions';

import { Redirect } from 'react-router-dom';

class SignIn extends Component {
  state = {
    email: ``,
    password: ``,
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
    this.props.signIn(this.state);
    this.props.history.push('/');
  }

  render() {
    const { authError, auth } = this.props;
    if(auth.uid) return <Redirect to='/' />
    return (
      <div className="form page">
        <form onSubmit={this.handleSubmit}>
          <h4 className="form__title">Log In</h4>
          <input
            type="email"
            name="email"
            id="email"
            onChange={this.handleChange}
            placeholder="Email"
            className="form__input"
            required
          />
          <input
            type="password"
            name="password"
            id="password"
            onChange={this.handleChange}
            placeholder="Password"
            className="form__input"
            required
          />
          <button className="form__cta">Log In</button>
          <div>
            { authError ? <p className="form__err">{ authError }</p> : null }
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError,
    auth: state.firebase.auth,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (creds) => dispatch(signIn(creds)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
