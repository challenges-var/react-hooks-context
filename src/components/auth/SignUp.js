import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { signUp } from '../../store/actions/authActions';

class SignUp extends Component {
  state = {
    email: ``,
    password: ``,
    firstName: ``,
    lastName: ``,
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state);
    this.props.signUp(this.state);
  }

  render() {
    const { auth, authError } = this.props;
    if(auth.uid) return <Redirect to='/' />
    return (
      <div className="form page">
        <form onSubmit={this.handleSubmit}>
          <h4 className="form__title">Sign Up</h4>
          <input
            type="text"
            id="firstName"
            onChange={this.handleChange}
            placeholder="First Name"
            className="form__input"
            required
          />
          <input
            type="text"
            id="lastName"
            onChange={this.handleChange}
            placeholder="LastName"
            className="form__input"
            required
          />
          <input
            type="email"
            name="email"
            id="email"
            onChange={this.handleChange}
            placeholder="Email"
            className="form__input"
            required
          />
          <input
            type="password"
            name="password"
            id="password"
            onChange={this.handleChange}
            placeholder="Password"
            className="form__input"
            required
          />
          <button className="form__cta">Sign Up</button>
          <div>
            { authError ? <p className="form__err">{ authError }</p> : null }
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    signUp: (newUser) => dispatch(signUp(newUser)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
