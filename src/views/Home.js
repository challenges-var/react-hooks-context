import React, { Component } from 'react'
import Dashboard from '../components/layout/Dashboard';

class Home extends Component {
  render() {
    return (
      <div className="group">
        <Dashboard />
      </div>
    );
  }
}

export default Home;