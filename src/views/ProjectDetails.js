import React from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
// import moment from 'moment';
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons';

import { Redirect } from 'react-router-dom';

const ProjectDetails = (props) => {
  const { project, auth } = props;
  if(!auth.uid) return <Redirect to='/signin' />
  if(project) {
    return (
      <section className="group">
        <div className="project">
          <Link to="/" className="project__link">
            <FontAwesomeIcon icon={faArrowAltCircleLeft} />&nbsp; Back
          </Link>
          <h3 className="project__title">Title: {project.title}</h3>
          {project.imgUrl ? <img src={project.imgUrl} alt="Post" className="project__img" /> : null }
          <p className="project__summary">{project.content} </p>
          {/* <small className="card__info">Created At {moment(project.createdAt.toDate()).calendar()}</small> */}
          <small className="card__info">Published by {project.authorFirstName}</small>
        </div>
      </section>
    )
  } else {
    return (
      <div className="container center">
        <h3>Loading projects...</h3>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;
  const projects = state.firestore.data.projects;
  // console.log(projects);
  const project = projects ? projects[id] : 'No project for the moment';
  return {
    project: project,
    auth: state.firebase.auth,
  }
}


export default compose(
  connect(mapStateToProps),
  firestoreConnect([ { collection: 'projects' } ])
)(ProjectDetails);
