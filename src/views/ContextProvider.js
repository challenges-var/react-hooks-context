import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import BookList from '../components/layout/BookList';
import ThemeContextProvider from '../context/ThemeContext';
import ThemeToggle from '../components/layout/ThemeToggle';

class ContextProvider extends Component {

  render() {
    const { auth } = this.props;
    if (!auth.uid) return <Redirect to='/signin' />

    return (
      <div className="group context">
        <ThemeContextProvider>
          <ThemeToggle />
          <BookList />
        </ThemeContextProvider>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
  }
}

export default connect(mapStateToProps)(ContextProvider);