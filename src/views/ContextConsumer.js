import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import ThemeContextProvider from '../context/ThemeContext';

import BookList2 from '../components/layout/BookList2';

class ContextConsumer extends Component {

  render() {
    const { auth } = this.props;
    if (!auth.uid) return <Redirect to='/signin' />

    return (
      <div className="group context">
        <ThemeContextProvider>
          <BookList2 />
        </ThemeContextProvider>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
  }
}

export default connect(mapStateToProps)(ContextConsumer);